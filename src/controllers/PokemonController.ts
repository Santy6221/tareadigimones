import { Request, Response } from "express";
import { PokemonI } from "../interfaces/PokemonInterfaces";
import { MonsterTypeI } from "../interfaces/MonsterTypeI";
import PokemonService from "../services/PokemonService";

export function getAll(_: any, res: Response) {
    const pokemons = PokemonService.getAll();
    res.status(200).json(pokemons);
}

export function get(req: Request, res: Response) {
    try {
        const id = req.params.id && +req.params.id || undefined;
        if(!id){ throw "Se requiere el ID del pokemon."}
        const pokemon = PokemonService.get(id);
        res.status(200).json(pokemon);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByName(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        if(!name){ throw "Se requiere el name del pokemon."}
        const pokemons = PokemonService.getByName(name);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByType(req: Request, res: Response) {
    try {
        const type = req.params.type && req.params.type || undefined;
        if(!type){ throw "Se requiere el Tipo del pokemon."}
        const pokemons = PokemonService.getByType(type);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getWinner(req: Request, res: Response){
    try{
        const id1 = req.params.pokemon1;
        const id2 = req.params.pokemon2;

        if(!id1 || !id2){ throw "Se requieren 2 id de pokemon."}
        const winner = PokemonService.getWinner(parseInt(id1), parseInt(id2));
        res.status(200).json(winner);
    }catch(error){
        res.status(400).send(error);
    }
}

export function addPokemon(req: Request, res: Response){
    try{

        if(!req.params.id || !req.params.name || !req.params.type || !req.params.imgurl){
            throw "los parametros no se llenaron";
        }

        let tipo: MonsterTypeI = {name:"", strongAgainst:[""], weakAgainst:[""]}

        if(req.params.type == "Fuego"){
            tipo = {name: "Fuego", strongAgainst: ["Planta"], weakAgainst: ["Agua"]}
        }
        if(req.params.type == "Agua"){
            tipo = {name: "Agua", strongAgainst: ["Fuego"], weakAgainst: ["Planta"]}
        }
        if(req.params.type == "Planta"){
            tipo = {name: "Planta", strongAgainst: ["Agua"], weakAgainst: ["Fuego"]}
        }
        const pokemon: PokemonI = {
            id: parseInt(req.params.id),
            name: req.params.name,
            type: [tipo],
            img: req.params.imgurl
        } 
        const created = PokemonService.addpokemon(pokemon);
        res.status(200).json(created);
    }catch(error){
        res.status(400).send(error);
    }
}
