import { Request, Response } from "express";
import { DigimonI } from "../interfaces/DigimonInterfaces";
import { MonsterTypeI } from "../interfaces/MonsterTypeI";
import DigimonsService from "../services/DigimonsService";

export function getAll(_: any, res: Response) {
    const digimons = DigimonsService.getAll();
    res.status(200).json(digimons);
}

export function get(req: Request, res: Response) {
    try {
        const id = req.params.id && +req.params.id || undefined;
        if(!id){ throw "Se requiere el ID del digimon."}
        const digimon = DigimonsService.get(id);
        res.status(200).json(digimon);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByName(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        if(!name){ throw "Se requiere el name del digimon."}
        const digimons = DigimonsService.getByName(name);
        res.status(200).json(digimons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByType(req: Request, res: Response) {
    try {
        const type = req.params.type && req.params.type || undefined;
        if(!type){ throw "Se requiere el Tipo del digimon."}
        const digimons = DigimonsService.getByType(type);
        res.status(200).json(digimons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getWinner(req: Request, res: Response){
    try{
        const id1 = req.params.digimon1;
        const id2 = req.params.digimon2;

        if(!id1 || !id2){ throw "Se requieren 2 id de digimon."}
        const winner = DigimonsService.getWinner(parseInt(id1), parseInt(id2));
        res.status(200).json(winner);
    }catch(error){
        res.status(400).send(error);
    }
}

export function addDigimon(req: Request, res: Response){
    try{

        if(!req.params.id || !req.params.name || !req.params.type || !req.params.imgurl){
            throw "los parametros no se llenaron";
        }

        let tipo: MonsterTypeI = {name:"", strongAgainst:[""], weakAgainst:[""]}

        if(req.params.type == "Vacuna"){
            tipo = {name: "Vacuna", strongAgainst: ["Virus"], weakAgainst: ["Data"]}
        }
        if(req.params.type == "Virus"){
            tipo = {name: "Virus", strongAgainst: ["Data"], weakAgainst: ["Vacuna"]}
        }
        if(req.params.type == "Data"){
            tipo = {name: "Data", strongAgainst: ["Vacuna"], weakAgainst: ["Virus"]}
        }
        const digimon: DigimonI = {
            id: parseInt(req.params.id),
            name: req.params.name,
            type: [tipo],
            img: req.params.imgurl
        } 
        const created = DigimonsService.addDigimon(digimon);
        res.status(200).json(created);
    }catch(error){
        res.status(400).send(error);
    }
}
