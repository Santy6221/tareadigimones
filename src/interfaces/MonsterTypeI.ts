export interface MonsterTypeI{
    name: string
    strongAgainst: Array<any>
    weakAgainst: Array<any>
}
