import { PokemonI } from "../interfaces/PokemonInterfaces";
const db = require('../db/Pokemons.json');

module PokemonService { 
    export function getAll(): Array<PokemonI> {
        const Pokemons: Array<PokemonI> = db;
        return Pokemons
    }
    export function get(id: number): PokemonI {
        const Pokemons: Array<PokemonI> = db;
        const pokemon: Array<PokemonI> = Pokemons.filter(e => e.id === id);
        if (pokemon.length < 1) {
            throw "No se encontró el pokemon"
        }
        return pokemon[0];
    }
    export function getByName(name: string): Array<PokemonI> {
        const Pokemons: Array<PokemonI> = db;
        const matches: Array<PokemonI> = Pokemons.filter(function(el) {
            return el.name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").indexOf(name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "")) > -1;
        })
        if (matches.length < 1) {
            throw "No se encontró el pokemon"
        }
        return matches;
    }
    
    export function getByType(type: string): Array<PokemonI> {
        const Pokemons: Array<PokemonI> = db;
        let matches: Array<PokemonI> = [];
        Pokemons.forEach(pokemon => {
            const found = pokemon.type.filter(e => e.name === type);
            if (found.length>0) {
                matches.push(pokemon);
            }
        })
         
        if (matches.length < 1) {
            throw "No se encontró el tipo"
        }
        return matches;
    }

    export function getWinner(id1: number, id2: number): PokemonI | string{
        let pokemon: Array<PokemonI> = [PokemonService.get(id1), PokemonService.get(id2)];
        if (pokemon.length < 2) {
            throw "No se encontró uno de los pokemon";
        }
        let weaknesess1: string;
        let weaknesess2: string;
        let tmp: Array<any> = [];
        pokemon[0].type.forEach(type =>{
            tmp.push(type.weakAgainst)
        });
        weaknesess1 = tmp.join();
        tmp = [];
        pokemon[1].type.forEach(type =>{
            tmp.push(type.weakAgainst)
        });
        weaknesess2 = tmp.join();
        console.log(weaknesess1);
        let points1 = 0;
        let points2 = 0;
        console.log(pokemon[0]);
        console.log(pokemon[1]);

        pokemon[0].type.forEach(type =>{
            if (type.name == weaknesess2){
                points1 += 1;
            }else{
                points2 += 1;
            }
        });

        if (points1 < points2){
            return pokemon[1];
        }
        if (points1 > points2){
            return pokemon[0];
        }
        if (points1 == points2){
            return "Empate";
        }

        return "NULL";
    }

    export function addpokemon(pokemon: PokemonI): Array<PokemonI>{
        const Pokemons: Array<PokemonI> = db;
        console.log(db);
        console.log(Pokemons);
        db.push(pokemon)
        return db
    }
}

export default PokemonService;
