import { DigimonI } from "../interfaces/DigimonInterfaces";
const db = require('../db/Digimons.json');

module DigimonsService { 
    export function getAll(): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        return digimons
    }
    export function get(id: number): DigimonI {
        const digimons: Array<DigimonI> = db;
        const digimon: Array<DigimonI> = digimons.filter(e => e.id === id);
        if (digimon.length < 1) {
            throw "No se encontró el digimon"
        }
        return digimon[0];
    }
    export function getByName(name: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        const matches: Array<DigimonI> = digimons.filter(function(el) {
            return el.name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").indexOf(name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "")) > -1;
        })
        if (matches.length < 1) {
            throw "No se encontró el digimon"
        }
        return matches;
    }
    
    export function getByType(type: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        let matches: Array<DigimonI> = [];
        digimons.forEach(digimon => {
            const found = digimon.type.filter(e => e.name === type);
            if (found.length>0) {
                matches.push(digimon);
            }
        })
         
        if (matches.length < 1) {
            throw "No se encontró el tipo"
        }
        return matches;
    }

    export function getWinner(id1: number, id2: number): DigimonI | string{
        let digimon: Array<DigimonI> = [DigimonsService.get(id1), DigimonsService.get(id2)];
        if (digimon.length < 2) {
            throw "No se encontró uno de los digimon";
        }
        let weaknesess1: string;
        let weaknesess2: string;
        let tmp: Array<any> = [];
        digimon[0].type.forEach(type =>{
            tmp.push(type.weakAgainst)
        });
        weaknesess1 = tmp.join();
        tmp = [];
        digimon[1].type.forEach(type =>{
            tmp.push(type.weakAgainst)
        });
        weaknesess2 = tmp.join();
        console.log(weaknesess1);
        let points1 = 0;
        let points2 = 0;
        console.log(digimon[0]);
        console.log(digimon[1]);

        digimon[0].type.forEach(type =>{
            if (type.name == weaknesess2){
                points1 += 1;
            }else{
                points2 += 1;
            }
        });

        if (points1 < points2){
            return digimon[1];
        }
        if (points1 > points2){
            return digimon[0];
        }
        if (points1 == points2){
            return "Empate";
        }

        return "NULL";
    }

    export function addDigimon(digimon: DigimonI): Array<DigimonI>{
        const digimons: Array<DigimonI> = db;
        console.log(db);
        console.log(digimons);
        db.push(digimon)
        return db
    }
}

export default DigimonsService;
