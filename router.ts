import express from 'express';
import * as DigimonsController from './src/controllers/DigimonsController';
import * as pokemonsController from './src/controllers/PokemonController';

export const router = express.Router();

router.get('/', (req, res) => {
    res.send('Hello World with Typescript!')
})

router.get('/ts', (req, res) => {
    res.send('Typescript es lo máximo!')
})

router.get('/digimons', DigimonsController.getAll);
router.get('/digimons/:id', DigimonsController.get);
router.get('/digimons/add/:id/:name/:type/:imgurl', DigimonsController.addDigimon);
router.get('/digimons/name/:name', DigimonsController.getByName);
router.get('/digimons/type/:type', DigimonsController.getByType);
router.get('/digimons/stronger/:digimon1/:digimon2', DigimonsController.getWinner);
router.get('/pokemons', pokemonsController.getAll);
router.get('/pokemons/:id', pokemonsController.get);
router.get('/pokemons/add/:id/:name/:type/:imgurl', pokemonsController.addPokemon);
router.get('/pokemons/name/:name', pokemonsController.getByName);
router.get('/pokemons/type/:type', pokemonsController.getByType);
router.get('/pokemons/stronger/:pokemon1/:pokemon2', pokemonsController.getWinner);

router.post("/", (req, res) => {
    console.log("Cuerpo:", req.body);
    res.status(200).send(req.body);
});
